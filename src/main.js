// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Mint from 'mint-ui';
import 'mint-ui/lib/style.css'
Vue.use(Mint);
// 5.0 注册mui的css样式
import '../static/mui/dist/css/mui.css';
import axios from 'axios'
import qs from 'qs'
Vue.prototype.axios = axios
Vue.config.productionTip = false
/*第一步安装 npm install moment --save
2,在需要用的组件中 datefmt('YYYY-MM-DD HH:mm:ss')
 */
axios.interceptors.request.use( (config) => {
  if (config.method=="post"){
    config.data = qs.stringify(config.data);
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }
  return config;
},  (error) => {
  return Promise.reject(error);
});
// 8.0 定义一个全局过滤器实现日期的格式化
// 使用momentjs这个日期格式化类库实现日的格式化功能
import moment from 'moment';
//vue全局注册
Vue.filter('datefmt',function(input,fmstring){
  return moment(input).format(fmstring);
});

// 9.0 使用图片预览组件
//   import VuePreview from 'vue-preview';
//   Vue.use(VuePreview);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
