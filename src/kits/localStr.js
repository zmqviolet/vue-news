/*负责操作localstronge的帮助文件
1,获取数据
localSytorage.getItem(key)
2，设置和追加数据
 localSytorage.setItem(key,value),value是一个字符串
3，移除数据
 localSytorage.removeItem(key) 根据key移除
* */
//1.0定义常量key。操作里面的数据都以key作为标识,相当于给存储的东西一个名字,为了保证key的唯一性，key使用时间,这里最好存储时间戳，将来方便转义和识别
export let KEY = 'gooddata';
//定义object的格式
export let valueObj = {goodid:0,count:0};

// 2.0 实现数据的增加
// value;格式： {goodsid:87,count:10
export function addData(value){
  //追加商品数量和id，存储的是json格式
  // 第一步先获取
  let jsondata= localStorage.getItem(KEY);
   jsondata = jsondata || '[]';
   let arr= JSON.parse(jsondata);
   //将可以的值保存的数组里面去
   arr.push(value);
  // 3.0 将arr 转换成json字符串保存起来，将对象变成字符串
  localStorage.setItem(KEY,JSON.stringify(arr));
}

// 3.0 获取数据
export function getItem(){
  let jsondata= localStorage.getItem(KEY);
  // 将json格式字符串转换成 js对象
  // jsonString：是一个标准的json格式
  jsondata = jsondata || '[]';
  return JSON.parse(jsondata);
}
// 4.0 移除数据
export function remoteItem(id){
    let obj = getItem();  //拿到的是数组，存的时候是以对象
    for(var i = obj.length - 1; i>=0; i--){
      if(obj[i].goodid = id){
        obj.splice(i,1);
      }
    }
}
// 5.0 将localStorage中的数据全部整合成一个对象的形式
/*
 * 格式：
 * {
 *  商品id的值：商品的购买数量,
 *  商品id的值：商品的购买数量
 * }
 * 真正的数据是：
 * {
 *   87:4,
 *   88:1
 * }
 * */

export  function getgoodObject(){
//获取数组
//  arr 的格式,这里就是将数组变成对象，并且去重
//   [{goodsid:87,count:1},{goodsid:87,count:3},{goodsid:88,count:1}]
//  取出换成数据
  let arr= getItem();
  let resObj = {};
  for(var i = 0;i<arr.length;i++){
    let item = arr[i];
    if(!resObj[item.goodid]){
    //  如果没有当前商品的数据，则添加一个数据
      resObj[item.goodid] = item.count;
    }else{
    //  已经有当前商品的数据，则加count追加
      let count = resObj[item.goodid];
      resObj[item.goodid] =  count + item.count;
    }
  }
  return resObj;
}

//更新数据
//obj的格式：{goodsid:87,type:'add'}
export function updateData(obj){
  let count = 1;
  let arr = getItem();
    if(obj.type=="add"){
    //  加
      arr.push({goodid:obj.goodid,count:count})
    }else{
        //减法
      for(var i =0 ;i < arr.length ; i++) {
        //如果这个对象中的count值等于1，则删除这个对象，否则将这个对象的count减1以后保存回去
        if(arr[i].goodid == obj.goodid) {
          if (arr[i].count > 1) {
            arr[i].count = arr[i].count - 1;
            break;
          } else {
            //删除此对象
            arr.splice(i, 1);
            break;
          }
        }
      }
    }
  //    将最新的arr保存回localStorage中
  localStorage.setItem(KEY,JSON.stringify(arr));
}

