/**
 * Created by SHILIANG on 2018/1/2.
 */

import Vue from 'vue'
import vueRouter from 'vue-router'
Vue.use(vueRouter)

/*一级路由*/
import Home from '../main/home'
import Mine from '../main/mine'
import Order from '../main/order'
import Search from '../main/search'
import NewsList from '../news/newslist'
import NewsInfo from '../news/newsinfo'
import PhotoList from '../photo/photoList'
import PhotoInfo from '../photo/photoinfo'
import GoodList from '../good/goodlist.vue'
import GoodInfo from '../good/goodinfo.vue'
import GoodDesc from '../good/goodsdesc.vue'
 import GoodComm from '../good/goodscomment.vue'
/*定义路由规则*/
const routes = [
  //服务器启动的时候，默认进入home
   //重定向的路由问题，刷新会拿不到数据
  {path: '/', redirect: '/main/home'},
  // {path:'',component:Home},
  {path:'/main/home',component:Home},
  {path:'/main/mine',component:Mine},
  {path:'/main/order',component:Order},
  {path:'/main/search',component:Search},
  {path:'/news/newslist',component:NewsList},
  {path:'/news/newsinfo/:id',component:NewsInfo},
  {path:'/photo/photoList',component:PhotoList},
  {path:'/photo/photoinfo/:id',component:PhotoInfo},
  {path:'/good/goodlist',component:GoodList},
  {path:'/good/goodinfo/:id',component:GoodInfo},
  {path:'/good/goodsdesc/:id',component:GoodDesc},
  {path:'/good/goodscomment/:id',component:GoodComm},

  // {path:'*',component:Home}
]

//通过 this.$route
/*创建路由实例*/
const router = new  vueRouter({
     // mode: 'history',
    linkActiveClass:'mui-active', //改变路由激活时的class名称
    routes
});

// router.beforeEach((to, from, next)=>{
//   console.error(to)
//   // next();
// })
export default router
